﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Metas : MonoBehaviour {

	public InputField input;
	private float valor;
	public Menus menu;

	// Use this for initialization
	void Start () {
		if(PlayerPrefs.GetFloat("Progreso2")==0){
			valor = 0;
		}else{
			valor = PlayerPrefs.GetFloat("Progreso2");
		}
	}

	public void enviar(){
		valor = valor + float.Parse(input.text);
		PlayerPrefs.SetFloat("Progreso2", valor);
		menu.Disponible = menu.Disponible - float.Parse(input.text);
		menu.Total = menu.Total - float.Parse(input.text);
		PlayerPrefs.SetFloat("Disponible",menu.Disponible);
		PlayerPrefs.SetFloat("Total",menu.Total);
		menu.DisponibleTxt.text = "$ "+ PlayerPrefs.GetFloat("Disponible");
		menu.TotalTxt.text = "$ " + PlayerPrefs.GetFloat("Total");
		input.text = "";
		GameObject.Find("PanelMetas").GetComponent<RectTransform>().localScale = Vector3.zero;
	}
}
