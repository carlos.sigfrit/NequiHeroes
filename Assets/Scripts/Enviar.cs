﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Enviar : MonoBehaviour {

	public InputField celular;
	public RectTransform panelEnviar;
	public RectTransform panelInsignias;
	public RectTransform panelInsignias2;

	public void abrirVentana(){
		panelEnviar.localScale = new Vector3(1,1,1);
	}

	public void cerrarVentana(){
		panelEnviar.localScale = Vector3.zero;
	}

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		verificarNumero();	
	}

	void verificarNumero(){
		if(celular.text == "3142114475"){
			panelInsignias2.localScale = Vector3.zero;
			panelInsignias.localScale = new Vector3(1,1,1);
		}else{
			if(celular.text == "3114892632"){
				panelInsignias.localScale = Vector3.zero;
				panelInsignias2.localScale = new Vector3(1,1,1);
			}else{
				panelInsignias2.localScale = Vector3.zero;
				panelInsignias.localScale = Vector3.zero;
			}
		}

	}
}
