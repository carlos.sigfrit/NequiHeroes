﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class Menus : MonoBehaviour {

    private GameObject panelMetas;
    private GameObject panelGuardaditos;
    private Image panelBolsillo;
    private GameObject panelPlus;
    private Image panelDinero;
	private GameObject panelTrofeos;
    private Image btnAtras1;
    private Image btnAtras2;
	private Image btnAtras3;
	private float v,v1,v2;
	private Logros logro1;
	private Logros logro2;
	private Logros logro3;
	public float Disponible;
	public float Total;
	public float DineroGuardadito;

	public Text TotalTxt;
	public Text DisponibleTxt;

	public Sprite SuperNequi;
	public Sprite SpriteMetas;
	public Sprite SpriteRecargas;
	public Sprite SpriteAhorrar;
	public Image insigniaAfuera;

	public RectTransform PanelFondoSN;
	public RectTransform PanelSN;

	public Text nombreInsignias;
	public GameObject prestamo;

	public bool SuperNequiActivo;

	private bool Ani1,Ani2, Ani3, Ani4;
	public GameObject vent1,vent2,vent3,vent4;

	void Update(){
		logro1.ajustarBarraProgreso();
		logro1.cambiarLabel();
		logro2.ajustarBarraProgreso();
		logro2.cambiarLabel();
		logro3.ajustarBarraProgreso();
		logro3.cambiarLabel();

		
		if(logro1.insigniaObtenida == true ){
			if(Ani1==false){
				vent1.SetActive(true);
				Ani1=true;
			}
			insigniaAfuera.sprite = SpriteAhorrar;
			nombreInsignias.text = "Ahorra-Man";
		}
		if(logro2.insigniaObtenida == true ){
			if(Ani2==false){
				vent2.SetActive(true);
				Ani2=true;
			}
			insigniaAfuera.sprite = SpriteRecargas;
			nombreInsignias.text = "Recarga-Girl";
		}
		if(logro3.insigniaObtenida == true ){
			if(Ani3==false){
				vent3.SetActive(true);
				Ani3=true;
			}
			insigniaAfuera.sprite = SpriteMetas;
			nombreInsignias.text = "Cumpli-Man";
		}
		if(logro1.insigniaObtenida == true && logro2.insigniaObtenida == true && logro3.insigniaObtenida == true){
			if(Ani4==false){
				vent4.SetActive(true);
				Ani4=true;
			}
			GameObject.Find("InsigniaFuera").GetComponent<Button>().interactable = true;
			insigniaAfuera.sprite = SuperNequi;
			SuperNequiActivo=true;
			nombreInsignias.text = "Super Nequi";
		}
	}

	public void borrarCache(){
		PlayerPrefs.DeleteAll();
		SceneManager.LoadScene("Main");
	}

	// Use this for initialization
    void Start () {
	
		if(PlayerPrefs.GetFloat("Disponible")==0){
			PlayerPrefs.SetFloat("Disponible",3000000f);
		}

		Disponible = PlayerPrefs.GetFloat("Disponible");


		if(PlayerPrefs.GetFloat("Total")==0){
			PlayerPrefs.SetFloat("Total",3100000f);
		}
		Total = PlayerPrefs.GetFloat("Total");

		if(PlayerPrefs.GetFloat("Guardadito")==0){
			PlayerPrefs.SetFloat("Guardadito",100000f);
		}
		DineroGuardadito = PlayerPrefs.GetFloat("Guardadito");


		TotalTxt.text = "$ " + Total;
		DisponibleTxt.text = "$ " + Disponible;


		logro1 = GameObject.Find("FondoLogro1").GetComponent<Logros>();
		logro2 = GameObject.Find("FondoLogro2").GetComponent<Logros>();
		logro3 = GameObject.Find("FondoLogro3").GetComponent<Logros>();


		panelTrofeos = GameObject.Find("PanelLogros");
        panelMetas = GameObject.Find("PanelMetas");
		panelGuardaditos = GameObject.Find("PanelGuardaditos");
        panelBolsillo = GameObject.Find("PanelBolsillo").GetComponent<Image>();
        panelPlus = GameObject.Find("PanelPlus");
        panelDinero = GameObject.Find("PanelDinero").GetComponent<Image>();
        btnAtras1 = GameObject.Find("BtnAtras1").GetComponent<Image>();
        btnAtras2 = GameObject.Find("BtnAtras2").GetComponent<Image>();
		btnAtras3 = GameObject.Find("BtnAtras3").GetComponent<Image>();
    }

	public void mostrarPanelSuperNequi(){
		PanelSN.localScale = new Vector3(1,1,1);
		PanelFondoSN.localScale = new Vector3(1,1,1);
	}

	public void cerrarPanelSuperNequi(){
		PanelSN.localScale = Vector3.zero;
		PanelFondoSN.localScale = Vector3.zero;
	}

    public void cambioVentana(int id)
    {
        switch (id)
        {
			//Metas
            case 1:
				if(PlayerPrefs.GetFloat("Progreso2")==null){
					v2=0;
				}else{
					v2 = PlayerPrefs.GetFloat("Progreso2");
				}
				PlayerPrefs.SetFloat("Progreso2",v2);
			panelMetas.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
			btnAtras1.enabled = true;
                break;
			//Guardadito
            case 2:
				if(PlayerPrefs.GetFloat("Progreso0")==0){
					v=0;
				}else{
					v = PlayerPrefs.GetFloat("Progreso0");
				}
				PlayerPrefs.SetFloat("Progreso0",v);
				panelGuardaditos.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
                btnAtras1.enabled = true;
                break;
            case 3:
                panelBolsillo.enabled = true;
                btnAtras1.enabled = true;
                break;
			//Recargas
            case 4:
				if(PlayerPrefs.GetFloat("Progreso1")==null){
					v1=0;
				}else{
					v1 = PlayerPrefs.GetFloat("Progreso1");
				}
				PlayerPrefs.SetFloat("Progreso1",v1);
				panelPlus.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
                btnAtras1.enabled = true;
                break;
            case 5:
				if(SuperNequiActivo==true){
					prestamo.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
				}
                panelDinero.enabled = true;
                btnAtras2.enabled = true;
                break;
			case 6:

				logro1.ajustarBarraProgreso();
				logro1.cambiarLabel();
				logro2.ajustarBarraProgreso();
				logro2.cambiarLabel();
				logro3.ajustarBarraProgreso();
				logro3.cambiarLabel();

				panelTrofeos.GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
				btnAtras3.enabled = true;
				break;
        }
    }

	public void entrarEnviar(){
		GameObject.Find("PanelDinero").transform.localScale = Vector3.zero;
		GameObject.Find ("PanelEnviar").transform.localScale = new Vector3(1,1,1);
	}

    public void cerrarVentanas(){
		if(SuperNequiActivo==true){
			prestamo.GetComponent<RectTransform>().localScale = Vector3.zero;
		}
		panelMetas.GetComponent<RectTransform>().localScale = Vector3.zero;
		panelBolsillo.enabled = false;
        panelDinero.enabled = false;
		panelGuardaditos.GetComponent<RectTransform>().localScale = Vector3.zero;
		panelPlus.GetComponent<RectTransform>().localScale = Vector3.zero;
		panelTrofeos.GetComponent<RectTransform>().localScale = Vector3.zero;
    }

	public void cerrarVentanasRedesSociales(){
		GameObject.Find("PanelInsignia").GetComponent<RectTransform>().localScale = Vector3.zero;
		GameObject.Find("VentanaInsignia1").GetComponent<RectTransform>().localScale = Vector3.zero;
		GameObject.Find("VentanaInsignia2").GetComponent<RectTransform>().localScale = Vector3.zero;
		GameObject.Find("VentanaInsignia3").GetComponent<RectTransform>().localScale = Vector3.zero;
	}
}
