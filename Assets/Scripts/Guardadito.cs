﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Guardadito : MonoBehaviour {

	private float v;
	private Logros logro1;
	public Text textoGuardadito;
	public InputField input;
	public Menus menu;

	// Use this for initialization
	void Start () {
		textoGuardadito.text = "$ "+ PlayerPrefs.GetFloat("Guardadito");
		logro1 = GameObject.Find("FondoLogro1").GetComponent<Logros>();
	}

	public void sumar(){
				menu.Disponible = menu.Disponible - float.Parse(input.text);
				PlayerPrefs.SetFloat("Disponible",menu.Disponible);
				menu.DineroGuardadito = menu.DineroGuardadito + float.Parse(input.text);
				PlayerPrefs.SetFloat("Guardadito",menu.DineroGuardadito);
				textoGuardadito.text = "$ "+ PlayerPrefs.GetFloat("Guardadito");
				menu.DisponibleTxt.text = "$ "+ PlayerPrefs.GetFloat("Disponible");
				guardarGuardadito();
				input.text = "0";
		
	}

	public void restar(){
		if(input.text == null){
			
		}else{
			if(float.Parse(input.text) <= PlayerPrefs.GetFloat("Guardadito")){
				menu.Disponible = menu.Disponible + float.Parse(input.text);
				PlayerPrefs.SetFloat("Disponible",menu.Disponible);
				menu.DineroGuardadito = menu.DineroGuardadito - float.Parse(input.text);
				PlayerPrefs.SetFloat("Guardadito",menu.DineroGuardadito);
				textoGuardadito.text = "$ "+ PlayerPrefs.GetFloat("Guardadito");
				menu.DisponibleTxt.text = "$ "+ PlayerPrefs.GetFloat("Disponible");
				input.text = "0";
			}else{
				
			}
		}
	}

	public void guardarGuardadito(){
		if(PlayerPrefs.GetFloat("Progreso0")==0){
			v=0;
		}else{
			v = PlayerPrefs.GetFloat("Progreso0");
		}
		v = v + float.Parse(input.text);
		PlayerPrefs.SetFloat("Progreso0",v);
		logro1.ajustarBarraProgreso();
		logro1.cambiarLabel();
	}
}
