﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Logros : MonoBehaviour {

	public int ID;
	public Sprite InsigniaActiva;
	public Slider barraDeProgreso;
	public Color gris;
	public Color rosa;
	public Image start1;
	public Image start2;
	public Image start3;
	public GameObject insignia;
	public Text Label;
	public string reto1;
	public string reto2;
	public string reto3;
	private bool estrella1;
	private bool estrella2;
	private bool estrella3;
	public bool insigniaObtenida;
	public int VMaximo1;
	public int VMaximo2;
	public int VMaximo3;
	private float porcentaje;
	private float valor;

	// Use this for initialization
	void Start () {
		start1.color = gris;
		start2.color = gris;
		start3.color = gris;
	}

	public void ajustarBarraProgreso(){
		valor = PlayerPrefs.GetFloat("Progreso"+ID);
		if(valor < VMaximo1){
			porcentaje = (100f/VMaximo1)*valor;
			barraDeProgreso.value = porcentaje;
		}else{
			estrella1=true;
			start1.color = rosa;
			if(valor < VMaximo2){
				porcentaje = (100f/VMaximo2)*valor;
				barraDeProgreso.value = porcentaje;
			}else{
				estrella2=true;
				start2.color = rosa;
				if(valor < VMaximo3){
					porcentaje = (100f/VMaximo3)*valor;
					barraDeProgreso.value = porcentaje;
				}else{
					barraDeProgreso.value = 100f;
					estrella3=true;
					start3.color = rosa;
				}
			}
		}
	}

	public void cambiarLabel(){
		if(estrella1 == false){
			Label.text = reto1;
		}else{
			if(estrella2 == false){
				Label.text = reto2;
			}else{
				if(estrella3 == false){
					Label.text = reto3;
				}else{
					Label.text = "¡Lo has logrado!";
					insignia.GetComponent<Image>().sprite = InsigniaActiva;
					insignia.GetComponent<Button>().interactable = true;
					insigniaObtenida = true;
				}
			}
		}
	}

	public void abrirInsignia(){
		GameObject.Find("PanelInsignia").GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
		switch(this.ID){
		case 0:
			GameObject.Find("VentanaInsignia1").GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
			break;
		case 1:
			GameObject.Find("VentanaInsignia2").GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
			break;
		case 2:
			GameObject.Find("VentanaInsignia3").GetComponent<RectTransform>().localScale = new Vector3(1,1,1);
			break;
		}
	}
	

}
